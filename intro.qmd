## Introduction

Frictionless Data [specification](https://specs.frictionlessdata.io/) defines two types of containers: *Data Package* and *Tabular Data Package*.
*Data Package* is a general container that can be used to store any type of data.
*Tabular Data Package* is a container that can **only** store CSV files.

The next few chapters will use minimal working example of *Data Package* and *Tabular Data Package*.

## *Data Package* Minimal Working Example

The minimal working example of *Data Package* is based on "Bacterial training dataset for Galaxy training network tutorials on Genome assembly" [@gladman2017] and has two Fastq sequence reads from an imaginary *Staphylococcus aureus* bacterium.

## *Tabular Data Package* Minimal Working Example

The minimal working example of *Tabular Data Package* is based on Edgar Anderson's Iris Data [@andersonIrisesGaspePeninsula1935] and has a single CSV file with measurements in centimeters of parts of flowers.
When needed, we will reference the copy in Zenodo [@anderson2018].
