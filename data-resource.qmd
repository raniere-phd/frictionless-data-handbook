---
engine: knitr
---

# Data Resource

*Data Resource* is a description of a single data resource, for example a FASTA file or a CSV file, and must be a valid JSON object that follow the [*Data Resource* JSON Schema](https://specs.frictionlessdata.io/schemas/data-resource.json).
Advantages of provide the data as a *Data Resource* include

-   single place for human metadata like title and description
-   single place for machine metadata like encoding

The smallest *Data Resource* is

::: {#fig-smallest-data-resource}
{{< include data-resource/smallest.json >}}

Smallest *Data Resource*.
:::

and only provide the access name for the resource and the location of the data.
A couple of properties are optional.

::: {#fig-small-data-resource}
{{< include data-resource/small.json >}}

*Data Resource* with some optional properties.
:::

## Sequence Reads from imaginary *Staphylococcus aureus* bacterium

```{bash}
#| eval: false
#| echo: false

frictionless describe --json --stats mutant_R1.fastq > data-resource/mutant_R1.json 
```

The imaginary *Staphylococcus aureus* [@gladman2017] is used in Galaxy training network tutorials.
The forward Fastq sequence reads can be describe as a *Data Resource* as

::: {#fig-iris-data-resource}
{{< include data-resource/mutant_R1.json >}}

*Data Resource* for forward Fastq sequence reads of imaginary *Staphylococcus aureus*.
:::

```{bash}
#| error: true

frictionless validate data-resource/mutant_R1.json
```

## Edgar Anderson's Iris

```{bash}
#| eval: false
#| echo: false

frictionless describe --json --stats iris.csv > data-resource/iris.json 
```

Edgar Anderson's iris data [@andersonIrisesGaspePeninsula1935] set is famous and used in many tutorials to the R programming language.
It can be describe as a *Data Resource* as

::: {#fig-iris-data-resource}
{{< include data-resource/iris.json >}}

*Data Resource* for Edgar Anderson's iris data.
:::

To validate the *Data Resource*, run

```{bash}
#| error: true

frictionless validate data-resource/iris.json
```
