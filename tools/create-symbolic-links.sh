#!/bin/bash
EXAMPLE_FOLDERS="data-resource data-package table-schema tabular-data-resource tabular-data-package"
for data_file in data/*.csv
do
    for example_folder in $EXAMPLE_FOLDERS
    do
	cd $example_folder
	ln -s ../$data_file ${data_file/data\//}
	cd ..
    done
done
