#!/bin/bash
#
# After run
#
#     $ docker-compose up --force-recreate --build
DOCKER_IMAGE="registry.gitlab.com/raniere-phd/frictionless-data-handbook"
docker compose build --no-cache --pull
docker image tag frictionless-data-handbook_quarto $DOCKER_IMAGE
docker push $DOCKER_IMAGE
