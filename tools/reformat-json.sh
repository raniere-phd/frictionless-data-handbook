#!/bin/bash
if [ -z $1 ]
then
    FILES=$(find . -type f -not -path '*/[._]*' -name '*.json')
else
    FILES=$1
fi

for file in $FILES
do
    python -m json.tool --sort-keys --indent 2 $file /tmp/clean.json
    sed 's/^/    /' /tmp/clean.json > $file
done
