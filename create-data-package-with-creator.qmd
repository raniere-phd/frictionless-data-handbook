# Create *Data Package* with Data Package Creator

[Data Package Creator](https://create.frictionlessdata.io/) is a website that you can used to create your *Data Package*.
If you have many files, we recommend you to use the command-line or a programming language like Python or R as demonstrated in the next chapters.

![Screenshot of https://create.frictionlessdata.io/ showing all three panels with Firefox 102.0 on 8 July 2022.](img/create.frictionlessdata.io/home-with-json.png){fig-align="center"}

Data Package Creator has three panels.
The left panel has the metadata of the data package like name and title.
The middle panel has the list of resources.
And the right panel has the data package as a JSON.

Fill the form in the left panel with the metadata below:

-   `Name`: `staphylococcus-aureus-reads`

-   `Title`: `Sequence Reads from an imaginary Staphylococcus aureus bacterium`

-   `Profile`: `Data Package`

-   `Description`: `Training dataset for Galaxy training network tutorials on Genome assembly`

-   `License`'s `Name`: `CC-BY-4.0`

![Screenshot of https://create.frictionlessdata.io/ after inclusion of metadata with Firefox 102.0 on 8 July 2022.](img/create.frictionlessdata.io/data-package-metadata.png){alt="Screenshot of https://create.frictionlessdata.io/ with Firefox 102.0 on 8 July 2022." fig-align="center"}

In the middle panel, right click in `Add resource`.

![Screenshot of https://create.frictionlessdata.io/ after add second resource with Firefox 102.0 on 8 July 2022.](img/create.frictionlessdata.io/data-package-two-resources.png){alt="Screenshot of https://create.frictionlessdata.io/ with Firefox 102.0 on 8 July 2022." fig-align="center"}

In the middle panel, fill the form of each one of the resources:

1.  `Name`: `mutant_r1`\
    `Path`: `https://zenodo.org/record/582600/files/mutant_R1.fastq?download=1`
2.  `Name`: `mutant_r2` \
    `Path`: `https://zenodo.org/record/582600/files/mutant_R2.fastq?download=1`

![Screenshot of https://create.frictionlessdata.io/ after resources' details with Firefox 102.0 on 8 July 2022.](img/create.frictionlessdata.io/data-package-with-resources.png){alt="Screenshot of https://create.frictionlessdata.io/ with Firefox 102.0 on 8 July 2022." fig-align="center"}

For each one of the resources, right click in the gear (⚙️) to access the resource's additional details.

![Screenshot of https://create.frictionlessdata.io/ after open resource's additional details with Firefox 102.0 on 8 July 2022.](img/create.frictionlessdata.io/data-package-gear.png){alt="Screenshot of https://create.frictionlessdata.io/ with Firefox 102.0 on 8 July 2022." fig-align="center"}

Fill the additional details form of each one of the resources:

1.  `Title`: `Forward Fastq sequence reads`\
    `Profile`: `Data Resource`\
    `Format`: `fastq`\
    `Description`: `Each read is 150 bases long.`
2.  `Title`: `Reverse Fastq sequence reads`\
    `Profile`: `Data Resource`\
    `Format`: `fastq`\
    `Description`: `Each read is 150 bases long.`

![Screenshot of https://create.frictionlessdata.io/ after provide all the data details with Firefox 102.0 on 8 July 2022.](img/create.frictionlessdata.io/data-package-complete.png)

Now, right click the `Download` button in the left panel and save the `datapackage.json`.
In the future, if you need to edit your *Data Package*, you can use the `Upload` function in the left panel.
