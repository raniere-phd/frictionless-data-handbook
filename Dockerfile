FROM mambaorg/micromamba AS quarto
USER root
RUN apt update \
    && apt install -y curl \
    && rm -rf /var/lib/apt/lists/* \
    && curl -L https://github.com/quarto-dev/quarto-cli/releases/download/v1.1.78/quarto-1.1.78-linux-amd64.deb -o /tmp/quarto.deb \
    && dpkg -i /tmp/quarto.deb \
    && rm /tmp/quarto.deb
USER $MAMBA_USER
COPY --chown=$MAMBA_USER:$MAMBA_USER env.yaml /tmp/env.yaml
RUN micromamba install -y -n base -f /tmp/env.yaml \
    && micromamba clean --all --yes \
    && rm -rf /opt/conda/conda-meta
WORKDIR /tmp/quarto
CMD quarto preview --port 4444 --no-browser --no-navigate

FROM quarto AS jupyter
RUN micromamba install -y -n base -c conda-forge jupyterlab jupytext jupyterlab_code_formatter black isort \
    && micromamba clean --all --yes \
    && rm -rf /opt/conda/conda-meta
WORKDIR /tmp/quarto
CMD quarto preview --port 4444 --no-browser --no-navigate
