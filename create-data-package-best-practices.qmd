---
engine: knitr
---

# *Data Package* Best Practices

## Prefer Remote Resources

*Data Package* can have resources from three sources:

-   local files
-   remote files
-   "inline" data

When sharing data, it's better that the *Data Package* only have remote files to ensure the *Data Package* is small and users can download it quickly.

## Compress CSV Files

When sharing data, compression makes storage and data transfer more cost-effective and sustainable.
Additionaly, users will experience a shorter download times.

To create a *Data Package* with compressed resources, update the following properties: `path`, `bytes`, and `hash` based on the compressed resources.

To extract data from a *Data Package* with compressed resources, proceed as usual.
For example, consider a version of *Tabular Data Package* for Edgar Anderson's iris data (@fig-iris-tabular-data-package) with compressed resources.

::: {#fig-diff-iris-tabular-data-package}
```{bash}
#| echo: false
#| error: true

diff tabular-data-package/iris.json tabular-data-package/iris.zip.json
```

Difference of *Tabular Data Package* for Edgar Anderson's iris data with (`<`) and without (`>`) compression.
:::

We can extract the data using

```{bash}
#| error: true

frictionless extract --type package --csv tabular-data-package/iris.zip.json
```

## Validate your Data Package

Check that your data is valid before sharing it.
