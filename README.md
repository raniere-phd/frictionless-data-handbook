# Frictionless Data Handbook

## Preview

### GitLab Pages

Visit https://raniere-phd.gitlab.io/frictionless-data-handbook

### Docker Compose

Run

```
$ docker compose up
```

and

visit http://localhost:4444/.